module.exports = {
  setupFiles: ['<rootDir>/__tests__/env.js'],
  collectCoverage: true,
  testMatch: [
    '**/__tests__/*.test.js',
  ],
  preset: '@shelf/jest-mongodb',
};
