const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY, {
  apiVersion: '2020-08-27',
  appInfo: { // For sample support and debugging, not required for production:
    name: "loc's stripe demo",
    version: '0.0.1',
  },
});

const { MongoClient } = require('mongodb');
const auth0 = require('./auth0');

// Create a module-scoped MongoClient promise.
// CRITICAL: You must call connect() outside the handler so that the client
// can be reused across function invocations.
// let client = new MongoClient(process.env.MONGODB_URI.replace(/['"]/g, ''),
const client = new MongoClient(
  `${process.env.MONGODB_URI}?retryWrites=true&w=majority`,
  { useNewUrlParser: true, useUnifiedTopology: true },
);
const clientPromise = client.connect();

exports.mongodb = clientPromise;

async function connectToDatabase() {
  // eslint-disable-next-line no-shadow
  const client = await clientPromise;
  return client.db().collection(process.env.MONGODB_COLLECTION);
}

async function createCheckoutSession(bundle) {
  const domainURL = process.env.DOMAIN;
  const items = JSON.parse(process.env.ITEMS.replace(/^['"]|['"]$/g, ''));
  // eslint-disable-next-line camelcase
  const line_items = items[bundle].map((price) => ({
    price,
    quantity: 1,
    adjustable_quantity: {
      enabled: true,
      minimum: 0,
      maximum: 10,
    },
  }));

  const session = await stripe.checkout.sessions.create({
    mode: 'payment',
    submit_type: 'donate',
    // customer_email: 'foo@example.com',
    // eslint-disable-next-line camelcase
    line_items,
    allow_promotion_codes: true,
    success_url: `${domainURL}/success.html?session_id={CHECKOUT_SESSION_ID}`,
    cancel_url: `${domainURL}/canceled.html`,
    // automatic_tax: { enabled: true }
  });

  // const response = {
  //   statusCode: 303,
  //   headers: {
  //     Location: session.url,
  //   },
  // };
  const response = {
    statusCode: 200,
    headers: {
      Location: session.url,
      'Access-Control-Allow-Origin': process.env.ALLOWED_ORIGIN,
    },
    body: JSON.stringify({ url: session.url }),
  };

  return response;
}

async function storeSession(session) {
  const coll = await connectToDatabase();
  await coll.insertOne({
    type: 'session',
    data: session,
  });
}

async function processWebhooks(event) {
  /*
  let ev;

  const sig = event.headers['Stripe-Signature'];
  try {
    ev = stripe.webhooks.constructEvent(
      JSON.stringify(event.body, null, 2),
      sig,
      process.env.STRIPE_SECRET_KEY);
  } catch (err) {
    return {
      statusCode: 400,
      body: `Webhook Error: ${err.message}`,
    }
  }
  */

  const ev = JSON.parse(event.body);

  const coll = await connectToDatabase();

  await coll.insertOne({
    type: 'webhook',
    data: ev,
  });

  const res = {
    statusCode: 200,
    body: JSON.stringify({
      type: ev.type,
      id: ev.id,
    }),
  };

  return res;
}

async function getCheckoutSession(sessionId) {
  // get checkout session from Stripe
  // the expansion mechanism from Stripe allows us to fetch more data about this session
  const session = await stripe.checkout.sessions.retrieve(sessionId, {
    expand: [
      'line_items',
      'line_items.data.price.product',
      'total_details.breakdown',
    ],
  });

  // store the session in our database
  await storeSession(session);

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': process.env.ALLOWED_ORIGIN,
    },
    body: JSON.stringify(session),
  };
}

exports.handler = async (event, context) => {
  // just in case someone gave us something bogus
  if (!event) {
    return null;
  }

  // authenticate
  if (event.type && event.type === 'TOKEN') {
    let data = null;
    try {
      data = await auth0.authenticate(event);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log(error);
      return context.fail('Unauthorized');
    }

    return data;
  }

  // web resources
  try {
    // eslint-disable-next-line no-console
    console.log(`resource: ${event.resource}, method: ${event.httpMethod}`);

    // create a checkout session
    if (event.resource === '/create-checkout-session' && event.httpMethod === 'POST') {
      return await createCheckoutSession(event.queryStringParameters.bundle);
    }

    // read back a checkout session
    if (event.resource === '/checkout-session' && event.httpMethod === 'GET') {
      return await getCheckoutSession(event.queryStringParameters.sessionId);
    }

    // handle webhooks asynchronously from Stripe
    if (event.resource === '/webhook' && event.httpMethod === 'POST') {
      return await processWebhooks(event);
    }

    // login using auth0
    if (event.resource === '/login' && event.httpMethod === 'GET') {
      return await auth0.login(event);
    }

    // unable to match a route / verb
    const error = new Error('unknown route / verb');
    error.code = 406;
    throw error;
  } catch (error) {
    // return an error response if errored out
    // eslint-disable-next-line no-console
    console.log(JSON.stringify(error));
    return {
      statusCode: error.code ? error.code : 500,
      body: error.message,
    };
  }
};
