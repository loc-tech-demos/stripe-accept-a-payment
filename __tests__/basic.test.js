const fs = require('fs');

let dbCon = null;

// mock out stripe
const sessionsMock = {
  retrieve: jest.fn((id) => {
    if (id === 'cs_test_throw_unknown_id') {
      throw new Error(`Invalid checkout.session id: ${id}`);
    }

    const session = JSON.parse(fs.readFileSync('events/session.json', 'utf8'));
    session.id = id;
    return session;
  }),

  create: jest.fn(() => {
    const session = JSON.parse(fs.readFileSync('events/session.json', 'utf8'));
    session.url = 'http://www.example.com';
    return session;
  }),
};

jest.doMock('stripe', () => jest.fn(() => ({
  checkout: {
    sessions: sessionsMock,
  },
})));

// requiring in our module after the mocks
const app = require('..');

beforeAll(async () => {
  // mongo-memory-server is setup using the `jest-mongodb` preset

  dbCon = await app.mongodb;
});

describe('get checkout session', () => {
  it('should get a valid session', async () => {
    const event = JSON.parse(fs.readFileSync('events/get-checkout-session.json'));

    const res = await app.handler(event);

    const body = JSON.parse(res.body);

    // checking the general characteristics of the session payload from Stripe
    // in particular, check for the data we are expanding for
    expect(res.statusCode).toEqual(200);
    expect(body.id).toEqual(event.queryStringParameters.sessionId);
    expect(body.line_items).toBeDefined();
    expect(body.line_items.data.length).toBeGreaterThan(0);
    expect(body.line_items.data[0].price.product.id).toBeDefined();
    expect(body.total_details.breakdown).toBeDefined();
  });

  it('should throw on unknown session id', async () => {
    const event = JSON.parse(fs.readFileSync('events/get-checkout-session.json'));
    event.queryStringParameters.sessionId = 'cs_test_throw_unknown_id';

    const res = await app.handler(event);
    expect(res.statusCode).toEqual(500);
  });
});

describe('processing webhooks', () => {
  it('should accept a valid webhook', async () => {
    const event = JSON.parse(fs.readFileSync('events/create-webhook-pi.json'));
    const eventBody = JSON.parse(event.body);

    const res = await app.handler(event);

    const body = JSON.parse(res.body);

    expect(res.statusCode).toEqual(200);
    expect(body.id).toEqual(eventBody.id);
    expect(body.type).toEqual('payment_intent.created');
  });

  it('should store webhook in database', async () => {
    const event = JSON.parse(fs.readFileSync('events/create-webhook-pi.json'));
    const eventBody = JSON.parse(event.body);

    // find document in database
    const client = dbCon.db().collection(process.env.MONGODB_COLLECTION);
    const res = await client.findOne({ type: 'webhook', 'data.type': 'payment_intent.created', 'data.id': eventBody.id });

    expect(res).toBeDefined();
    expect(res.type).toEqual('webhook');
    expect(res.data).toEqual(eventBody);
  });

  it('should reject items w/ valid signature', async () => {
    // TODO: add signature handler
  });
});

describe('create checkout session', () => {
  it('should create a valid session', async () => {
    const event = JSON.parse(fs.readFileSync('events/create-checkout-session-la.json'));

    const res = await app.handler(event);

    expect(res.statusCode).toEqual(200);

    const body = JSON.parse(res.body);
    expect(body.url).toEqual('http://www.example.com');
  });
});

describe('unknown route/verb', () => {
  it('should return 406 with unknown resource', async () => {
    // load a legit event, but mess up the route
    const event = JSON.parse(fs.readFileSync('events/create-checkout-session-la.json'));
    event.resource = 'foo';

    const res = await app.handler(event);

    expect(res.statusCode).toEqual(406);
  });

  it('should return 406 with unknown verb', async () => {
    // load a legit event, but mess up the verb
    const event = JSON.parse(fs.readFileSync('events/create-checkout-session-la.json'));
    event.httpMethod = 'PATCH';

    const res = await app.handler(event);

    expect(res.statusCode).toEqual(406);
  });

  it('should return 500 with internal error', async () => {
    const res = await app.handler(null);

    expect(res).toBeNull();
  });
});

afterAll(async () => {
  if (dbCon) {
    await dbCon.close();
  }
});
