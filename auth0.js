require('dotenv').config({ silent: true });
const axios = require('axios');

const jwksClient = require('jwks-rsa');
const jwt = require('jsonwebtoken');
const util = require('util');

const client = jwksClient({
  cache: true,
  rateLimit: true,
  jwksRequestsPerMinute: 10, // Default value
  jwksUri: process.env.JWKS_URI,
});

const getPolicyDocument = (effect, resource) => {
  const policyDocument = {
    Version: '2012-10-17', // default version
    Statement: [
      {
        Action: 'execute-api:Invoke', // default action
        Effect: effect,
        Resource: resource,
      },
    ],
  };
  return policyDocument;
};

// extract and return the Bearer Token from the Lambda event parameters
const getToken = (params) => {
  if (!params.type || params.type !== 'TOKEN') {
    throw new Error('Expected "event.type" parameter to have value "TOKEN"');
  }

  const tokenString = params.authorizationToken;
  if (!tokenString) {
    throw new Error('Expected "event.authorizationToken" parameter to be set');
  }

  const match = tokenString.match(/^Bearer (.*)$/);
  if (!match || match.length < 2) {
    throw new Error(`Invalid Authorization token - ${tokenString} does not match 'Bearer .*'`);
  }
  return match[1];
};

const jwtOptions = {
  audience: process.env.AUDIENCE,
  issuer: process.env.TOKEN_ISSUER,
};

module.exports.authenticate = (params) => {
  // eslint-disable-next-line no-console
  console.log(params);
  const token = getToken(params);

  const decoded = jwt.decode(token, { complete: true });
  if (!decoded || !decoded.header || !decoded.header.kid) {
    throw new Error('invalid token');
  }

  const getSigningKey = util.promisify(client.getSigningKey);
  return getSigningKey(decoded.header.kid)
    .then((key) => {
      const signingKey = key.publicKey || key.rsaPublicKey;
      return jwt.verify(token, signingKey, jwtOptions);
    })
    .then((decodedData) => ({
      principalId: decodedData.sub,
      policyDocument: getPolicyDocument('Allow', params.methodArn),
      context: { scope: decodedData.scope },
    }));
};

module.exports.login = async (params) => {
  const data = {
    grant_type: 'authorization_code',
    client_id: process.env.CLIENT_ID,
    client_secret: process.env.CLIENT_SECRET,
    code: params.queryStringParameters.code,
    redirect_uri: `${process.env.AUDIENCE}/login`,
  };

  const response = await axios.post(
    `${process.env.TOKEN_ISSUER}oauth/token`,
    data,
    { headers: { 'content-type': 'application/json' } },
  );

  return {
    statusCode: 302,
    headers: {
      'Set-Cookie': `Bearer=${response.data.access_token}; Domain=accept-a-payment.demo.locborgtus.net; Secure`,
      Location: process.env.DOMAIN,
    //   'Set-Cookie': `Bearer=${response.data.access_token}`,
    //   Location: 'http://accept-a-payment.demo.locborgtus.net.s3-website-us-west-1.amazonaws.com',
    },
  };
};
