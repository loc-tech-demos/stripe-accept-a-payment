FROM public.ecr.aws/lambda/nodejs:14

COPY index.js auth0.js package.json package-lock.json ${LAMBDA_TASK_ROOT}/

# Install NPM dependencies for function
RUN NODE_ENV=production npm ci

# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
CMD [ "index.handler" ]  
