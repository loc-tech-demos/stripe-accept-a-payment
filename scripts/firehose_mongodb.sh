#!/bin/sh

if [ "$#" -lt 1 ]; then
  echo "Usage: $0 <AWS Kinesis Firehose file> <op type {session, webhook, list}>"
  exit 1
fi

KFH_FILE=$1
TYPE="${2:-session}"

if [ ${TYPE} == "list" ]; then
  cat ${KFH_FILE} | jq '.detail.fullDocument.data.data.object.id'
else
  cat ${KFH_FILE} | jq ".detail.fullDocument | select(.type==\"${TYPE}\") .data"
fi
