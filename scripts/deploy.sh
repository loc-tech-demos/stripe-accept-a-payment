source .env && \
  docker tag $IMAGE_NAME:latest $ECR_REPO/$IMAGE_NAME:latest && \
  docker push $ECR_REPO/$IMAGE_NAME:latest && \
  aws lambda update-function-code --function-name $LAMBDA_NAME --image-uri $ECR_REPO/$IMAGE_NAME:latest && \
  aws s3 sync ./client $S3_BUCKET --delete && \
  aws lambda wait function-updated --function-name $LAMBDA_NAME
