const urlParams = new URLSearchParams(window.location.search);
const sessionId = urlParams.get('session_id');

if (sessionId) {
  fetch(`https://api.accept-a-payment.demo.locborgtus.net/checkout-session?sessionId=${sessionId}`)
    .then((result) => result.json())
    .then((session) => {
      const sessionJSON = JSON.stringify(session, null, 2);
      document.querySelector('pre').textContent = sessionJSON;

      // populate line items
      const lineItems = document.getElementById('line_items');

      session.line_items.data.forEach((i) => {
        const div = document.createElement('ul');

        const img = document.createElement('img');
        [img.src] = i.price.product.images;
        img.width = 50;
        div.appendChild(img);

        div.appendChild(document.createTextNode(`Ride: ${i.price.product.name}, amount: $${(i.quantity * i.price.unit_amount) / 100}, `
          + `qty: ${i.quantity}, computed entries: ${i.quantity * i.price.product.metadata.entries}`));

        lineItems.appendChild(div);
      });
    })
    .catch((err) => {
      // eslint-disable-next-line no-console
      console.log('Error when fetching Checkout session', err);
    });
}
