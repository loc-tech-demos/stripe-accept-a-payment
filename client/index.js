// eslint-disable-next-line no-unused-vars
const buyBundle = async (bundle) => {
  const token = Cookies.get('Bearer');

  if (!token) {
    // eslint-disable-next-line no-alert
    window.alert('Log in first');
    return;
  }

  // this part is really tricky
  // because we have an auth token, make sure AWS API Gateway has CORS enbabled for this resource
  // in particular, this will create an OPTIONS resource where the browser will do a preflight check
  // see this: https://stackoverflow.com/questions/61703954/chrome-is-ignoring-access-control-allow-origin-header-and-fails-cors-with-prefli
  const url = `https://api.accept-a-payment.demo.locborgtus.net/create-checkout-session/?bundle=${bundle}`;
  const options = {
    method: 'POST',
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'text/plain',
    },
  };

  // make the post!
  const response = await fetch(url, options);

  // we receive a json object with a url field
  // that is the stripe cart we have to load
  const responseData = await response.json();

  // load the cart
  window.location.href = responseData.url;
};

async function getUserProfile() {
  const token = Cookies.get('Bearer');
  const url = 'https://dev-fgda3k69.us.auth0.com/userinfo';
  const options = {
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'text/plain',
    },
  };

  const response = await fetch(url, options);
  const profile = await response.json();

  return profile;
}

window.onload = async () => {
  const token = Cookies.get('Bearer');

  const div = document.getElementById('auth');
  while (div.firstChild) {
    div.removeChild(div.firstChild);
  }

  const redirectUri = encodeURIComponent('https://api.accept-a-payment.demo.locborgtus.net/login');
  const returnToUri = encodeURIComponent('https://accept-a-payment.demo.locborgtus.net');
  // const returnToUri = encodeURIComponent('http://accept-a-payment.demo.locborgtus.net.s3-website-us-west-1.amazonaws.com/');

  const a = document.createElement('a');
  a.style = 'padding: 10px; background: lightgray;';

  if (token) {
    // currently signed in
    const profile = await getUserProfile();

    a.appendChild(document.createTextNode(`Hi ${profile.name} - Sign Out`));
    a.onclick = () => {
      Cookies.remove('Bearer', { path: '/', domain: document.location.host });
      a.href = `https://dev-fgda3k69.us.auth0.com/v2/logout?client_id=XBc4rF5CSNCmhAbKgquA4ARX9oWV8ibn&returnTo=${returnToUri}`;
    };

    div.appendChild(a);
  } else {
    // currently signed out
    a.href = `https://dev-fgda3k69.us.auth0.com/authorize?response_type=code&client_id=XBc4rF5CSNCmhAbKgquA4ARX9oWV8ibn&audience=https%3A%2F%2Fapi.accept-a-payment.demo.locborgtus.net&state=xyzABC123&scope=openid+profile&redirect_uri=${redirectUri}`;
    a.style = 'padding: 10px; background: lightgray;';
    a.appendChild(document.createTextNode('Sign In'));

    div.appendChild(a);
  }
};
