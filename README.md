# stripe-accept-a-payment

Stripe "accept-a-payment" demo, with a serverless architecture

---

## Basic Architecture

AWS services used:

* API Gateway
* Lambda
* S3
* Route 53
* IAM (CICD policy)
* AWS Certificate Manager (ACM)
* ECR

---

## Configuration

### Tools Required

* aws cli
* nodejs
* docker

### Environment file configuration

Make a copy of `.env-sample` to `.env`

---

## Development Workflow

1. Update your code
1. Build an image:

  `npm run build_image`

2. Start the container:

  `npm start`

3. Trigger the handler with an event. Something like:

`curl -XGET "http://localhost:9000/2015-03-31/functions/function/invocations" -d @events/get-checkout-session.json`

where `get-checkout-session.json` is an example event payload coming from API Gateway.

---

## AWS Lambda workflow

This is useful for developing and pushing on the desktop

### Configure your env file

Ensure the ECR repo is created. Something like:

`aws ecr create-repository --repository-name hello-world --image-scanning-configuration scanOnPush=true --image-tag-mutability MUTABLE`

### Source your env file

`source .env`

### Login to ECR

You'll have to do this periodically. Make sure your AWS creds are up to date.

`aws ecr get-login-password --region us-west-1 | docker login --username AWS --password-stdin ${ECR_REPO}`

### Build

`npm run build_image`

### (Optional) Test your image

To manually test, do something like:

`curl -XPOST "http://localhost:9000/2015-03-31/functions/function/invocations" -d @foo.json`

Where `foo.json` is a sample file of what API Gateway sends to the Lambda.

### Deploy

`npm run deploy`

---
